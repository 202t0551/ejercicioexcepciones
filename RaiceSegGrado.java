import java.util.*;
// clase para representar cualquier ecuaci�n de segundo grado
class RaiceSegGrado{
  private double a,b,c;
  private double r1,r2;   
  public RaiceSegGrado(double a, double b, double c){
    this.a = a;
    this.b = b;
    this.c = c;
  }
  
  void raices() throws NoRaizRealException, CoefAceroException{
  double discr;
  if(b*b < 4*a*c)
    throw new NoRaizRealException("Discriminante negativo",a,b,c);
  if(a == 0)
    throw new CoefAceroException("No ecuaciones De segundo grado  ");
  discr = Math.sqrt(b*b - 4*a*c);
  r1 = (-b - discr) / (2*a);
  r2 = (-b + discr) / (2*a);
  }
  
  public void escribir(){
    System.out.println("Raices de la ecuaci�n; r1 = "+ (float)r1 + "  r2 = " + (float)r2);
  }
} 