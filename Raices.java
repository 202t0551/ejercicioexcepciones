import java.util.*;
// clase principal
public class Raices
{
  public static void main(String [] ar)
  {
    RaiceSegGrado rc;
    double a,b,c;
    Scanner entrada = new Scanner(System.in);
    // entrada de coeficientes de la ecuaci�n
    System.out.println("Coeficientes de ecuaci�n de segundo grado");
    System.out.println(" a = ");
    a = entrada.nextDouble();
    System.out.println(" b = ");
    b = entrada.nextDouble ();
    System.out.print(" c = ");
    c = entrada.nextDouble();
    // crea objeto ecuaci�n y bloque para captura de excepciones
    try
    {
      rc = new RaiceSegGrado(a,b,c);  
      rc.raices();
      rc.escribir();
    }
    catch(NoRaizRealException er)
    {
      System.out.println(er.getMessage());
    }
    catch(CoeficienteAcero er)
    {
      System.out.println(er.getMessage());
    }
  }
}