import java.util.*;
// representa la excepci�n: no es ecuaci�n de segundo grado
class CoefAceroException extends Exception
{
  public CoefAceroException(String m)
  {
    super(m);
  }
}